This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## General
The main component is the __Carousel__ that is embedded into the reusable __Section__ component. It is possible to click through the carousel forwards or backwards in a loop.

Data for the carousel could be set from an API in the __componentDidMount__ lifecycle method instead of having the state object prepopulated. This is just the case here for demonstration purposes of reusable components.


## Implementation Details
The __Carousel__ component holds the state and the function implementations that are called in child components. Those child components are stateless. Not all components that could be written as functional components are written as such and use the class syntax.

The __Section__ component could be reused throughout other parts of this or another bigger project.

This is also true fore the __Button__ and __Card__ components.
Styling via props could be integrated through different ways. 
For example Styled Components could be used to simplify the process and prevent errors.

Styling can be found in the App.css - BEM notation used for class names.

View the [carousel.png](https://gitlab.com/neslihangenctuerk/carousel-section/-/blob/master/carousel.png) for a static screen preview.

__next possible TODO__: 
+ Work on reponsiveness of the design.


## Installation and Usage

### `npm install`


### `npm start`
This runs the app in development mode.
Visit [http://localhost:3000](http://localhost:3000) in the browser.
Check this [Link](https://developers.google.com/fonts/faq) for privacy questions on font imports before you run the project via this script. The fonts are not crucial to the functionality of the app.


## Tools
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). Which is licensed under: [LICENSE](https://github.com/facebook/create-react-app/blob/master/LICENSE). 
Also there is a copy of this license under __NOTICE.md__ in this project folder.



2 Fonts are imported via [Google Fonts](https://fonts.google.com/) - licenses included in each one: 
[Playfair Display](https://fonts.google.com/specimen/Playfair+Display)
[Nunito](https://fonts.google.com/specimen/Nunito)
or check the __NOTICE.md__ for a direct link.
Check this [Link](https://developers.google.com/fonts/faq) for privacy questions on Font imports before you run the project via the script above. The fonts are not crucial to the functionality of the app.

Please also check the __NOTICE.md__ for more info on licenses.