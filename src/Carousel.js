import React from 'react'
import Card from './Card'
import Button from './Button'


class Carousel extends React.Component {

    state = {
        currentCard: 0,
        listOfCards: [
            {
                id: 0,
                title: 'Frontend Engineering',
                desc: 'Vue.js, Vuex, React.js, ES6, CSS/SCSS, BEM, Styled Components'
            },
            {
                id: 1,
                title: 'UX Design',
                desc: 'UX Research, Figma, Adobe XD, Illustrator, Inkscape, Gimp, Lightroom, Photoshop'
            },
            {
                id: 2,
                title: 'Programming Languages',
                desc: 'ES6, Python, Kotlin, Java, C++'
            }
        ]
    }

    showNextCard = () => {
        this.setState((state) => {
            if (state.currentCard < state.listOfCards.length - 1) {
                return {
                    currentCard: state.currentCard + 1 
                }
            } else {
                return {
                    currentCard: 0 
                }
            }
            
        })
    }


    showPrevCard = () => {
        this.setState((state) => {
            if(state.currentCard === 0) {
                return {
                    currentCard: state.listOfCards.length - 1
                }
            } else {
                return {
                    currentCard: state.currentCard - 1
                }
            }
        })
    }

    render() {
        const { listOfCards, currentCard } = this.state

        return (
            <div>
                
                <Card 
                    iconUrl={listOfCards[currentCard].iconUrl}
                    title={listOfCards[currentCard].title}
                    desc={listOfCards[currentCard].desc}
                    cardToDisplay={currentCard}
                />

                <div className="carousel__btn">
                    <Button 
                        onClick={this.showPrevCard} 
                        title={"Previous"} 
                    />

                    <Button 
                        onClick={this.showNextCard} 
                        title={"Next"} 
                    />
                </div>

                
                
            </div>
        )
    }
}

export default Carousel