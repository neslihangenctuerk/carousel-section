import React from 'react'


class Card extends React.Component {
 

    render() {
        const { title, desc } = this.props
        return (
            <div className="card">
                <div className="card__title">{title}</div>
                <div className="card__divider"></div>
                <div className="card__desc">{desc}</div>
            </div>
        )
    }
}

export default Card
