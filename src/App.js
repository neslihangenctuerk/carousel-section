import React from 'react'
import './App.css'

import Section from './Section'
import Carousel from './Carousel'

class App extends React.Component {

  render() {
    return (
      <div>

        <Section componentToBeDisplayed={<Carousel/>}/>

      </div>
    )
  }
  
}

export default App;
