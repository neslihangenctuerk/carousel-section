import React from 'react'


const Section = props => (

        <div className="section">
            {props.componentToBeDisplayed}
        </div>
            
)

    
export default Section